package com.fc;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Solution {

    public static void main(String[] args) {
        ArrayList<String> arrayList= new Solution().Permutation("qwertyuio");
        System.out.println(arrayList.size());
    }
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * @param str string字符串
     * @return string字符串ArrayList
     */
    private Set<String> set=new HashSet<>();
    private   ArrayList<String> list=new ArrayList<>();


    public ArrayList<String> Permutation(String str) {
        // write code here
        digui(str.toCharArray(),0,set);
        list.addAll(set);
        return list;
    }

    private void digui(char[] chars,Integer index,Set<String> set){
        if(index==chars.length-1){
            set.add(String.valueOf(chars));
        }else{
            for(int i=index;i<chars.length;i++){
                char[] charsTemp=chars.clone();
                char temp=charsTemp[i];
                charsTemp[i]=charsTemp[index];
                charsTemp[index]=temp;
                digui(charsTemp,index+1,set);
            }
        }
    }



}