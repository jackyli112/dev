package com.fc.test.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.test.common.base.BaseService;
import com.fc.test.common.support.ConvertUtil;
import com.fc.test.mapper.auto.DeviceUserMapper;
import com.fc.test.model.auto.DeviceUser;
import com.fc.test.model.auto.DeviceUserExample;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.util.SnowflakeIdWorker;
import com.fc.test.util.StringUtils;

import javax.annotation.Resource;

/**
 * 普通用户 DeviceUserService
 * @Title: DeviceUserService.java 
 * @Package com.fc.test.service 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:14  
 **/
@Service
public class DeviceUserService implements BaseService<DeviceUser, DeviceUserExample>{
	@Resource
	private DeviceUserMapper deviceUserMapper;
	
      	   	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<DeviceUser> list(Tablepar tablepar,DeviceUser record){
	        DeviceUserExample testExample=new DeviceUserExample();
	        if(tablepar.getSearchText()!=null){
	        	record.setUsername(tablepar.getSearchText());
			}
	        testExample.createCriteria().andLikeQuery(record);
	        testExample.setOrderByClause("id ASC");
			if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        }
	        PageHelper.startPage(tablepar.getPageNum(), tablepar.getPageSize());
	        List<DeviceUser> list= deviceUserMapper.selectByExample(testExample);
	        PageInfo<DeviceUser> pageInfo = new PageInfo<DeviceUser>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			List<String> lista=ConvertUtil.toListStrArray(ids);
			DeviceUserExample example=new DeviceUserExample();
			example.createCriteria().andIdIn(lista);
			return deviceUserMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public DeviceUser selectByPrimaryKey(String id) {
				
			return deviceUserMapper.selectByPrimaryKey(id);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(DeviceUser record) {
		return deviceUserMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(DeviceUser record) {
				
		//添加雪花主键id
		record.setId(SnowflakeIdWorker.getUUID());
			
				
		return deviceUserMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(DeviceUser record, DeviceUserExample example) {
		
		return deviceUserMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(DeviceUser record, DeviceUserExample example) {
		
		return deviceUserMapper.updateByExample(record, example);
	}

	@Override
	public List<DeviceUser> selectByExample(DeviceUserExample example) {
		
		return deviceUserMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(DeviceUserExample example) {
		
		return deviceUserMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(DeviceUserExample example) {
		
		return deviceUserMapper.deleteByExample(example);
	}
	
	/**
	 * 检查name
	 * @param deviceUser
	 * @return
	 */
	public int checkNameUnique(DeviceUser deviceUser){
		DeviceUserExample example=new DeviceUserExample();
		example.createCriteria().andUsernameEqualTo(deviceUser.getUsername());
		List<DeviceUser> list=deviceUserMapper.selectByExample(example);
		return list.size();
	}


}
