package com.fc.test.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.test.common.base.BaseService;
import com.fc.test.common.support.ConvertUtil;
import com.fc.test.mapper.auto.DeviceContactMapper;
import com.fc.test.model.auto.DeviceContact;
import com.fc.test.model.auto.DeviceContactExample;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.util.SnowflakeIdWorker;
import com.fc.test.util.StringUtils;

import javax.annotation.Resource;

/**
 * 设备联系人 DeviceContactService
 * @Title: DeviceContactService.java 
 * @Package com.fc.test.service 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:19  
 **/
@Service
public class DeviceContactService implements BaseService<DeviceContact, DeviceContactExample>{
	@Resource
	private DeviceContactMapper deviceContactMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<DeviceContact> list(Tablepar tablepar,DeviceContact record){
	        DeviceContactExample testExample=new DeviceContactExample();
	        if(tablepar.getSearchText()!=null){
	        	record.setDeviceName(tablepar.getSearchText());
			}
	        testExample.createCriteria().andLikeQuery(record);
	        testExample.setOrderByClause("id ASC");
			if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        }
	        PageHelper.startPage(tablepar.getPageNum(), tablepar.getPageSize());
	        List<DeviceContact> list= deviceContactMapper.selectByExample(testExample);
	        PageInfo<DeviceContact> pageInfo = new PageInfo<DeviceContact>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			List<String> lista=ConvertUtil.toListStrArray(ids);
			DeviceContactExample example=new DeviceContactExample();
			example.createCriteria().andIdIn(lista);
			return deviceContactMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public DeviceContact selectByPrimaryKey(String id) {
				
			return deviceContactMapper.selectByPrimaryKey(id);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(DeviceContact record) {
		return deviceContactMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(DeviceContact record) {
				
		//添加雪花主键id
		record.setId(SnowflakeIdWorker.getUUID());
			
				
		return deviceContactMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(DeviceContact record, DeviceContactExample example) {
		
		return deviceContactMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(DeviceContact record, DeviceContactExample example) {
		
		return deviceContactMapper.updateByExample(record, example);
	}

	@Override
	public List<DeviceContact> selectByExample(DeviceContactExample example) {
		
		return deviceContactMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(DeviceContactExample example) {
		
		return deviceContactMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(DeviceContactExample example) {
		
		return deviceContactMapper.deleteByExample(example);
	}
	
	/**
	 * 检查name
	 * @param deviceContact
	 * @return
	 */
	public int checkNameUnique(DeviceContact deviceContact){
		DeviceContactExample example=new DeviceContactExample();
		example.createCriteria().andDeviceIdEqualTo(deviceContact.getDeviceId());
		List<DeviceContact> list=deviceContactMapper.selectByExample(example);
		return list.size();
	}


}
