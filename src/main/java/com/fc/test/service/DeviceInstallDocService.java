package com.fc.test.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.test.common.base.BaseService;
import com.fc.test.common.support.ConvertUtil;
import com.fc.test.mapper.auto.DeviceInstallDocMapper;
import com.fc.test.model.auto.DeviceInstallDoc;
import com.fc.test.model.auto.DeviceInstallDocExample;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.util.SnowflakeIdWorker;
import com.fc.test.util.StringUtils;

import javax.annotation.Resource;

/**
 * 设备安装文档 DeviceInstallDocService
 * @Title: DeviceInstallDocService.java 
 * @Package com.fc.test.service 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:02  
 **/
@Service
public class DeviceInstallDocService implements BaseService<DeviceInstallDoc, DeviceInstallDocExample>{
	@Resource
	private DeviceInstallDocMapper deviceInstallDocMapper;
	
      	   	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<DeviceInstallDoc> list(Tablepar tablepar,DeviceInstallDoc record){
	        DeviceInstallDocExample testExample=new DeviceInstallDocExample();
		 if(tablepar.getSearchText()!=null){
			 record.setDeviceName(tablepar.getSearchText());
		 }
	        testExample.createCriteria().andLikeQuery(record);
	        testExample.setOrderByClause("id ASC");
			if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        }
	        PageHelper.startPage(tablepar.getPageNum(), tablepar.getPageSize());
	        List<DeviceInstallDoc> list= deviceInstallDocMapper.selectByExample(testExample);
	        PageInfo<DeviceInstallDoc> pageInfo = new PageInfo<DeviceInstallDoc>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			List<String> lista=ConvertUtil.toListStrArray(ids);
			DeviceInstallDocExample example=new DeviceInstallDocExample();
			example.createCriteria().andIdIn(lista);
			return deviceInstallDocMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public DeviceInstallDoc selectByPrimaryKey(String id) {
				
			return deviceInstallDocMapper.selectByPrimaryKey(id);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(DeviceInstallDoc record) {
		return deviceInstallDocMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(DeviceInstallDoc record) {
				
		//添加雪花主键id
		record.setId(SnowflakeIdWorker.getUUID());
			
				
		return deviceInstallDocMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(DeviceInstallDoc record, DeviceInstallDocExample example) {
		
		return deviceInstallDocMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(DeviceInstallDoc record, DeviceInstallDocExample example) {
		
		return deviceInstallDocMapper.updateByExample(record, example);
	}

	@Override
	public List<DeviceInstallDoc> selectByExample(DeviceInstallDocExample example) {
		
		return deviceInstallDocMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(DeviceInstallDocExample example) {
		
		return deviceInstallDocMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(DeviceInstallDocExample example) {
		
		return deviceInstallDocMapper.deleteByExample(example);
	}
	
	/**
	 * 检查name
	 * @param deviceInstallDoc
	 * @return
	 */
	public int checkNameUnique(DeviceInstallDoc deviceInstallDoc){
		DeviceInstallDocExample example=new DeviceInstallDocExample();
		example.createCriteria().andDeviceNameEqualTo(deviceInstallDoc.getDeviceName());
		List<DeviceInstallDoc> list=deviceInstallDocMapper.selectByExample(example);
		return list.size();
	}


}
