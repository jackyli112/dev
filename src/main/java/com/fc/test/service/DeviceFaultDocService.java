package com.fc.test.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.test.common.base.BaseService;
import com.fc.test.common.support.ConvertUtil;
import com.fc.test.mapper.auto.DeviceFaultDocMapper;
import com.fc.test.model.auto.DeviceFaultDoc;
import com.fc.test.model.auto.DeviceFaultDocExample;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.util.SnowflakeIdWorker;
import com.fc.test.util.StringUtils;

import javax.annotation.Resource;

/**
 * 设备故障文档 DeviceFaultDocService
 * @Title: DeviceFaultDocService.java 
 * @Package com.fc.test.service 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:45:49  
 **/
@Service
public class DeviceFaultDocService implements BaseService<DeviceFaultDoc, DeviceFaultDocExample>{
	@Resource
	private DeviceFaultDocMapper deviceFaultDocMapper;
	
      	   	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<DeviceFaultDoc> list(Tablepar tablepar,DeviceFaultDoc record){
	        DeviceFaultDocExample testExample=new DeviceFaultDocExample();
		 if(tablepar.getSearchText()!=null){
			 record.setDeviceName(tablepar.getSearchText());
		 }
	        testExample.createCriteria().andLikeQuery(record);
	        testExample.setOrderByClause("id ASC");
			if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        }
	        PageHelper.startPage(tablepar.getPageNum(), tablepar.getPageSize());
	        List<DeviceFaultDoc> list= deviceFaultDocMapper.selectByExample(testExample);
	        PageInfo<DeviceFaultDoc> pageInfo = new PageInfo<DeviceFaultDoc>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			List<String> lista=ConvertUtil.toListStrArray(ids);
			DeviceFaultDocExample example=new DeviceFaultDocExample();
			example.createCriteria().andIdIn(lista);
			return deviceFaultDocMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public DeviceFaultDoc selectByPrimaryKey(String id) {
				
			return deviceFaultDocMapper.selectByPrimaryKey(id);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(DeviceFaultDoc record) {
		return deviceFaultDocMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(DeviceFaultDoc record) {
				
		//添加雪花主键id
		record.setId(SnowflakeIdWorker.getUUID());
			
				
		return deviceFaultDocMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(DeviceFaultDoc record, DeviceFaultDocExample example) {
		
		return deviceFaultDocMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(DeviceFaultDoc record, DeviceFaultDocExample example) {
		
		return deviceFaultDocMapper.updateByExample(record, example);
	}

	@Override
	public List<DeviceFaultDoc> selectByExample(DeviceFaultDocExample example) {
		
		return deviceFaultDocMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(DeviceFaultDocExample example) {
		
		return deviceFaultDocMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(DeviceFaultDocExample example) {
		
		return deviceFaultDocMapper.deleteByExample(example);
	}
	
	/**
	 * 检查name
	 * @param deviceFaultDoc
	 * @return
	 */
	public int checkNameUnique(DeviceFaultDoc deviceFaultDoc){
		DeviceFaultDocExample example=new DeviceFaultDocExample();
		example.createCriteria().andDeviceNameEqualTo(deviceFaultDoc.getDeviceName());
		List<DeviceFaultDoc> list=deviceFaultDocMapper.selectByExample(example);
		return list.size();
	}


}
