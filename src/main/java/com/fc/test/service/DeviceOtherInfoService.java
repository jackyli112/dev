package com.fc.test.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.fc.test.common.base.BaseService;
import com.fc.test.common.support.ConvertUtil;
import com.fc.test.mapper.auto.DeviceOtherInfoMapper;
import com.fc.test.model.auto.DeviceOtherInfo;
import com.fc.test.model.auto.DeviceOtherInfoExample;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.util.SnowflakeIdWorker;
import com.fc.test.util.StringUtils;

import javax.annotation.Resource;

/**
 * 设备与单位信息 DeviceOtherInfoService
 * @Title: DeviceOtherInfoService.java 
 * @Package com.fc.test.service 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:09  
 **/
@Service
public class DeviceOtherInfoService implements BaseService<DeviceOtherInfo, DeviceOtherInfoExample>{
	@Resource
	private DeviceOtherInfoMapper deviceOtherInfoMapper;
	
      	   	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<DeviceOtherInfo> list(Tablepar tablepar,DeviceOtherInfo record){
	        DeviceOtherInfoExample testExample=new DeviceOtherInfoExample();
		 if(tablepar.getSearchText()!=null){
			 record.setName(tablepar.getSearchText());
		 }
	        testExample.createCriteria().andLikeQuery(record);
	        testExample.setOrderByClause("id ASC");
			if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        }
	        PageHelper.startPage(tablepar.getPageNum(), tablepar.getPageSize());
	        List<DeviceOtherInfo> list= deviceOtherInfoMapper.selectByExample(testExample);
	        PageInfo<DeviceOtherInfo> pageInfo = new PageInfo<DeviceOtherInfo>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			List<String> lista=ConvertUtil.toListStrArray(ids);
			DeviceOtherInfoExample example=new DeviceOtherInfoExample();
			example.createCriteria().andIdIn(lista);
			return deviceOtherInfoMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public DeviceOtherInfo selectByPrimaryKey(String id) {
				
			return deviceOtherInfoMapper.selectByPrimaryKey(id);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(DeviceOtherInfo record) {
		return deviceOtherInfoMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(DeviceOtherInfo record) {
				
		//添加雪花主键id
		record.setId(SnowflakeIdWorker.getUUID());
			
				
		return deviceOtherInfoMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(DeviceOtherInfo record, DeviceOtherInfoExample example) {
		
		return deviceOtherInfoMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(DeviceOtherInfo record, DeviceOtherInfoExample example) {
		
		return deviceOtherInfoMapper.updateByExample(record, example);
	}

	@Override
	public List<DeviceOtherInfo> selectByExample(DeviceOtherInfoExample example) {
		
		return deviceOtherInfoMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(DeviceOtherInfoExample example) {
		
		return deviceOtherInfoMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(DeviceOtherInfoExample example) {
		
		return deviceOtherInfoMapper.deleteByExample(example);
	}
	
	/**
	 * 检查name
	 * @param deviceOtherInfo
	 * @return
	 */
	public int checkNameUnique(DeviceOtherInfo deviceOtherInfo){
		DeviceOtherInfoExample example=new DeviceOtherInfoExample();
		example.createCriteria().andNameEqualTo(deviceOtherInfo.getName());
		List<DeviceOtherInfo> list=deviceOtherInfoMapper.selectByExample(example);
		return list.size();
	}


}
