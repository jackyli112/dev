package com.fc.test.model.auto;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 设备故障文档 DeviceFaultDoc 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:45:49
 */
 @ApiModel(value="DeviceFaultDoc", description="设备故障文档")
public class DeviceFaultDoc implements Serializable {

	private static final long serialVersionUID = 1L;
	
		
	/** 主键 **/
	@ApiModelProperty(value = "主键")
	private String id;
		
	/** 设备id **/
	@ApiModelProperty(value = "设备id")
	private String deviceId;
		
	/** 设备名称 **/
	@ApiModelProperty(value = "设备名称")
	private String deviceName;
		
	/** 文档名称 **/
	@ApiModelProperty(value = "文档名称")
	private String docName;
		
	/** 文档内容 **/
	@ApiModelProperty(value = "文档内容")
	private String docContent;
		
	/** 视频地址 **/
	@ApiModelProperty(value = "视频地址")
	private String videoUrl;
		
	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
		
	/** 备注 **/
	@ApiModelProperty(value = "备注")
	private String remark;
		
	/** 扩展备注 **/
	@ApiModelProperty(value = "扩展备注")
	private String remarkExtend;
		
		
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	 
			
	public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
	 
			
	public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
	 
			
	public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }
	 
			
	public String getDocContent() {
        return docContent;
    }

    public void setDocContent(String docContent) {
        this.docContent = docContent;
    }
	 
			
	public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
	 
			
	public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	 
			
	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
	 
			
	public String getRemarkExtend() {
        return remarkExtend;
    }

    public void setRemarkExtend(String remarkExtend) {
        this.remarkExtend = remarkExtend;
    }
	 
			
	public DeviceFaultDoc() {
        super();
    }
    
																																															
	public DeviceFaultDoc(String id,String deviceId,String deviceName,String docName,String docContent,String videoUrl,Date createTime,String remark,String remarkExtend) {
	
		this.id = id;
		this.deviceId = deviceId;
		this.deviceName = deviceName;
		this.docName = docName;
		this.docContent = docContent;
		this.videoUrl = videoUrl;
		this.createTime = createTime;
		this.remark = remark;
		this.remarkExtend = remarkExtend;
		
	}
	
}