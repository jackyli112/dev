package com.fc.test.model.auto;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 设备联系人 DeviceContact 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:19
 */
 @ApiModel(value="DeviceContact", description="设备联系人")
public class DeviceContact implements Serializable {

	private static final long serialVersionUID = 1L;
	
		
	/** 主键 **/
	@ApiModelProperty(value = "主键")
	private String id;
		
	/** 设备id **/
	@ApiModelProperty(value = "设备id")
	private String deviceId;
		
	/** 设备名称 **/
	@ApiModelProperty(value = "设备名称")
	private String deviceName;
		
	/** 联系人名称 **/
	@ApiModelProperty(value = "联系人名称")
	private String contactName;
		
	/** 工厂名称 **/
	@ApiModelProperty(value = "工厂名称")
	private String factory;
		
	/** 联系人电话 **/
	@ApiModelProperty(value = "联系人电话")
	private String contactPhone;
		
	/** 联系人微信 **/
	@ApiModelProperty(value = "联系人微信")
	private String contactWechat;
		
	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
		
	/** 备注 **/
	@ApiModelProperty(value = "备注")
	private String remark;
		
	/** 扩展备注 **/
	@ApiModelProperty(value = "扩展备注")
	private String remarkExtend;
		
		
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	 
			
	public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
	 
			
	public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
	 
			
	public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
	 
			
	public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }
	 
			
	public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }
	 
			
	public String getContactWechat() {
        return contactWechat;
    }

    public void setContactWechat(String contactWechat) {
        this.contactWechat = contactWechat;
    }
	 
			
	public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	 
			
	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
	 
			
	public String getRemarkExtend() {
        return remarkExtend;
    }

    public void setRemarkExtend(String remarkExtend) {
        this.remarkExtend = remarkExtend;
    }
	 
			
	public DeviceContact() {
        super();
    }
    
																																																				
	public DeviceContact(String id,String deviceId,String deviceName,String contactName,String factory,String contactPhone,String contactWechat,Date createTime,String remark,String remarkExtend) {
	
		this.id = id;
		this.deviceId = deviceId;
		this.deviceName = deviceName;
		this.contactName = contactName;
		this.factory = factory;
		this.contactPhone = contactPhone;
		this.contactWechat = contactWechat;
		this.createTime = createTime;
		this.remark = remark;
		this.remarkExtend = remarkExtend;
		
	}
	
}