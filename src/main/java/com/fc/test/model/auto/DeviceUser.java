package com.fc.test.model.auto;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 普通用户 DeviceUser 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:14
 */
 @ApiModel(value="DeviceUser", description="普通用户")
public class DeviceUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
		
	/** 主键 **/
	@ApiModelProperty(value = "主键")
	private String id;
		
	/** 账号 **/
	@ApiModelProperty(value = "账号")
	private String username;
		
	/** 密码 **/
	@ApiModelProperty(value = "密码")
	private String password;
		
	/** 单位id **/
	@ApiModelProperty(value = "单位id")
	private String companyId;
		
	/** 单位名称 **/
	@ApiModelProperty(value = "单位名称")
	private String companyName;
		
	/** 状态 **/
	@ApiModelProperty(value = "状态")
	private String status;
		
	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
		
	/** 备注 **/
	@ApiModelProperty(value = "备注")
	private String remark;
		
	/** 扩展备注 **/
	@ApiModelProperty(value = "扩展备注")
	private String remarkExtend;
		
		
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	 
			
	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
	 
			
	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
	 
			
	public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }


	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
	 
			
	public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	 
			
	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
	 
			
	public String getRemarkExtend() {
        return remarkExtend;
    }

    public void setRemarkExtend(String remarkExtend) {
        this.remarkExtend = remarkExtend;
    }
	 
			
	public DeviceUser() {
        super();
    }
    
																																															
	public DeviceUser(String id,String username,String password,String companyId,String companyName,String status,Date createTime,String remark,String remarkExtend) {
	
		this.id = id;
		this.username = username;
		this.password = password;
		this.companyId = companyId;
		this.companyName = companyName;
		this.status = status;
		this.createTime = createTime;
		this.remark = remark;
		this.remarkExtend = remarkExtend;
		
	}
	
}