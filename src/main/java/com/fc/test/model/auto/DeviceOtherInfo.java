package com.fc.test.model.auto;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 设备与单位信息 DeviceOtherInfo 
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:09
 */
 @ApiModel(value="DeviceOtherInfo", description="设备与单位信息")
public class DeviceOtherInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
		
	/** 主键 **/
	@ApiModelProperty(value = "主键")
	private String id;
		
	/** 名称 **/
	@ApiModelProperty(value = "名称")
	private String name;
		
	/** 数据类型(设备或单位) **/
	@ApiModelProperty(value = "数据类型(设备或单位)")
	private String type;
		
	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
		
	/** 备注 **/
	@ApiModelProperty(value = "备注")
	private String remark;
		
	/** 扩展备注 **/
	@ApiModelProperty(value = "扩展备注")
	private String remarkExtend;
		
		
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	 
			
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
	 
			
	public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
	 
			
	public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	 
			
	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
	 
			
	public String getRemarkExtend() {
        return remarkExtend;
    }

    public void setRemarkExtend(String remarkExtend) {
        this.remarkExtend = remarkExtend;
    }
	 
			
	public DeviceOtherInfo() {
        super();
    }
    
																																
	public DeviceOtherInfo(String id,String name,String type,Date createTime,String remark,String remarkExtend) {
	
		this.id = id;
		this.name = name;
		this.type = type;
		this.createTime = createTime;
		this.remark = remark;
		this.remarkExtend = remarkExtend;
		
	}
	
}