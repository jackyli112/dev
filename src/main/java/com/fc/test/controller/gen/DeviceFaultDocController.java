package com.fc.test.controller.gen;

import com.fc.test.model.auto.*;
import com.fc.test.service.DeviceOtherInfoService;
import com.fc.test.service.DeviceUserService;
import com.fc.test.shiro.util.ShiroUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import com.fc.test.service.DeviceFaultDocService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;

@Api(value = "设备故障文档")
@Controller
@CrossOrigin
@RequestMapping("/DeviceFaultDocController")
public class DeviceFaultDocController extends BaseController{
	
	private String prefix = "gen/deviceFaultDoc";
	@Autowired
	private DeviceFaultDocService deviceFaultDocService;
	@Autowired
	private DeviceOtherInfoService deviceOtherInfoService;
	@Autowired
	private DeviceUserService deviceUserService;







	@ApiOperation(value = "详情页面", notes = "详情页面")
	@GetMapping("/detail")
	public String detail(String id,ModelMap model)
	{
		model.put("data",deviceFaultDocService.selectByPrimaryKey(id));
		return prefix + "/detail";
	}


	/**
	 * 分页跳转
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:deviceFaultDoc:view")
    public String view(ModelMap model)
    {
		String username=ShiroUtils.getUser().getUsername();
		DeviceUserExample deviceUserExample=new DeviceUserExample();
		deviceUserExample.createCriteria().andUsernameEqualTo(username);
		List<DeviceUser> deviceUsers=deviceUserService.selectByExample(deviceUserExample);
		if(deviceUsers.size()!=0){
			model.put("flag",true);
		}else{
			model.put("flag",false);
		}
		DeviceOtherInfoExample deviceOtherInfoExample=new DeviceOtherInfoExample();
		deviceOtherInfoExample.createCriteria().andTypeEqualTo("设备");
		List<DeviceOtherInfo> list=deviceOtherInfoService.selectByExample(deviceOtherInfoExample);
		model.put("deviceList",list);


		String str="设备故障文档";
		setTitle(model, new TitleVo("列表", str+"管理", true,"欢迎进入"+str+"页面", true, false));
        return prefix + "/list";
    }
	
	/**
	 * 分页查询
	 */
	//@Log(title = "设备故障文档集合查询", action = "111")
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@PostMapping("/list")
	@RequiresPermissions("gen:deviceFaultDoc:list")
	@ResponseBody
	public Object list(Tablepar tablepar,DeviceFaultDoc record){
		PageInfo<DeviceFaultDoc> page=deviceFaultDocService.list(tablepar,record) ; 
		TableSplitResult<DeviceFaultDoc> result=new TableSplitResult<DeviceFaultDoc>(page.getPageNum(), page.getTotal(), page.getList()); 
		return  result;
	}
	
	/**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		DeviceOtherInfoExample deviceOtherInfoExample=new DeviceOtherInfoExample();
		deviceOtherInfoExample.createCriteria().andTypeEqualTo("设备");
		List<DeviceOtherInfo> list=deviceOtherInfoService.selectByExample(deviceOtherInfoExample);
		modelMap.put("deviceList",list);
        return prefix + "/add";
    }
	
	/**
     * 新增
     */
	//@Log(title = "设备故障文档新增", action = "111")
   	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:deviceFaultDoc:add")
	@ResponseBody
	public AjaxResult add(DeviceFaultDoc deviceFaultDoc){
		DeviceOtherInfo deviceOtherInfo=deviceOtherInfoService.selectByPrimaryKey(deviceFaultDoc.getDeviceId());
		deviceFaultDoc.setDeviceName(deviceOtherInfo.getName());
		int b=deviceFaultDocService.insertSelective(deviceFaultDoc);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "设备故障文档删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@PostMapping("/remove")
	@RequiresPermissions("gen:deviceFaultDoc:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=deviceFaultDocService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 检查Name
	 * @param tsysUser
	 * @return
	 */
	@ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
	@PostMapping("/checkNameUnique")
	@ResponseBody
	public int checkNameUnique(DeviceFaultDoc deviceFaultDoc){
		int b=deviceFaultDocService.checkNameUnique(deviceFaultDoc);
		if(b>0){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
		DeviceOtherInfoExample deviceOtherInfoExample=new DeviceOtherInfoExample();
		deviceOtherInfoExample.createCriteria().andTypeEqualTo("设备");
		List<DeviceOtherInfo> list=deviceOtherInfoService.selectByExample(deviceOtherInfoExample);
		mmap.put("deviceList",list);
        mmap.put("DeviceFaultDoc", deviceFaultDocService.selectByPrimaryKey(id));
        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "设备故障文档修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:deviceFaultDoc:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DeviceFaultDoc record)
    {
		DeviceOtherInfo deviceOtherInfo=deviceOtherInfoService.selectByPrimaryKey(record.getDeviceId());
		record.setDeviceName(deviceOtherInfo.getName());
        return toAjax(deviceFaultDocService.updateByPrimaryKeySelective(record));
    }

    
    /**
   	 * 根据主键查询
   	 * 
   	 * @param id
   	 * @param mmap
   	 * @return
   	 */
   	@ApiOperation(value = "根据id查询唯一", notes = "根据id查询唯一")
   	@PostMapping("/get/{id}")
   	public DeviceFaultDoc edit(@PathVariable("id") String id) {
   		return deviceFaultDocService.selectByPrimaryKey(id);
   	}
    

	
}
