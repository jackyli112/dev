package com.fc.test.controller.gen;

import com.fc.test.common.support.ConvertUtil;
import com.fc.test.model.auto.*;
import com.fc.test.service.DeviceOtherInfoService;
import com.fc.test.service.SysPermissionService;
import com.fc.test.service.SysUserService;
import com.fc.test.shiro.util.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageInfo;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import com.fc.test.service.DeviceUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Arrays;
import java.util.List;

@Api(value = "普通用户")
@Controller
@RequestMapping("/DeviceUserController")
public class DeviceUserController extends BaseController{
	
	private String prefix = "gen/deviceUser";
	@Autowired
	private DeviceUserService deviceUserService;
	@Autowired
	private DeviceOtherInfoService deviceOtherInfoService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysPermissionService sysPermissionService;

    @RequestMapping("/updateMenu")
	public  String  updateMenu(String ycd,String xcd,ModelMap model){
    	TsysPermissionExample example=new TsysPermissionExample();
		example.createCriteria().andNameEqualTo(ycd);
		List<TsysPermission> permissions=sysPermissionService.selectByExample(example);
		if(permissions.size()!=0){
			TsysPermission permission=permissions.get(0);
			permission.setName(xcd);
			sysPermissionService.updateByPrimaryKey(permission);
		}


    	return view(model);
	}
	
	/**
	 * 分页跳转
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:deviceUser:view")
    public String view(ModelMap model)
    {
		String username= ShiroUtils.getUser().getUsername();
		DeviceUserExample deviceUserExample=new DeviceUserExample();
		deviceUserExample.createCriteria().andUsernameEqualTo(username);
		List<DeviceUser> deviceUsers=deviceUserService.selectByExample(deviceUserExample);
		if(deviceUsers.size()!=0){
			model.put("flag",true);
		}else{
			model.put("flag",false);
		}

		String str="普通用户";
		setTitle(model, new TitleVo("列表", str+"管理", true,"欢迎进入"+str+"页面", true, false));
        return prefix + "/list";
    }
	
	/**
	 * 分页查询
	 */
	//@Log(title = "普通用户集合查询", action = "111")
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@PostMapping("/list")
	@RequiresPermissions("gen:deviceUser:list")
	@ResponseBody
	public Object list(Tablepar tablepar,DeviceUser record){
		PageInfo<DeviceUser> page=deviceUserService.list(tablepar,record) ; 
		TableSplitResult<DeviceUser> result=new TableSplitResult<DeviceUser>(page.getPageNum(), page.getTotal(), page.getList()); 
		return  result;
	}
	
	/**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
		DeviceOtherInfoExample deviceOtherInfoExample=new DeviceOtherInfoExample();
		deviceOtherInfoExample.createCriteria().andTypeEqualTo("单位");
		List<DeviceOtherInfo> list=deviceOtherInfoService.selectByExample(deviceOtherInfoExample);
		modelMap.put("companyList",list);

        return prefix + "/add";
    }
	
	/**
     * 新增
     */
	//@Log(title = "普通用户新增", action = "111")
   	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:deviceUser:add")
	@ResponseBody
	public AjaxResult add(DeviceUser deviceUser){


   		//707847376370536448 设备普通用户
		TsysUser tsysUser=new TsysUser();
		tsysUser.setUsername(deviceUser.getUsername());
		tsysUser.setPassword(deviceUser.getPassword());
		tsysUser.setNickname(deviceUser.getUsername());
        sysUserService.insertUserRoles(tsysUser, Arrays.asList("707847376370536448"));
		deviceUser.setCompanyName(deviceOtherInfoService.selectByPrimaryKey(deviceUser.getCompanyId()).getName());
		int b=deviceUserService.insertSelective(deviceUser);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "普通用户删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@PostMapping("/remove")
	@RequiresPermissions("gen:deviceUser:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=deviceUserService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 检查Name
	 * @param tsysUser
	 * @return
	 */
	@ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
	@PostMapping("/checkNameUnique")
	@ResponseBody
	public int checkNameUnique(DeviceUser deviceUser){
		int b=deviceUserService.checkNameUnique(deviceUser);
		if(b>0){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("DeviceUser", deviceUserService.selectByPrimaryKey(id));
		DeviceOtherInfoExample deviceOtherInfoExample=new DeviceOtherInfoExample();
		deviceOtherInfoExample.createCriteria().andTypeEqualTo("单位");
		List<DeviceOtherInfo> list=deviceOtherInfoService.selectByExample(deviceOtherInfoExample);
		mmap.put("companyList",list);

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "普通用户修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:deviceUser:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DeviceUser record)
    {
		TsysUserExample example=new TsysUserExample();
		example.createCriteria().andUsernameEqualTo(record.getUsername());
		TsysUser tsysUser=sysUserService.selectByExample(example).get(0);
		tsysUser.setPassword(record.getPassword());
		sysUserService.updateUserPassword(tsysUser);
		record.setCompanyName(deviceOtherInfoService.selectByPrimaryKey(record.getCompanyId()).getName());
        return toAjax(deviceUserService.updateByPrimaryKeySelective(record));
    }

    
    /**
   	 * 根据主键查询
   	 * 
   	 * @param id
   	 * @param mmap
   	 * @return
   	 */
   	@ApiOperation(value = "根据id查询唯一", notes = "根据id查询唯一")
   	@PostMapping("/get/{id}")
   	public DeviceUser edit(@PathVariable("id") String id) {
   		return deviceUserService.selectByPrimaryKey(id);
   	}
    

	
}
