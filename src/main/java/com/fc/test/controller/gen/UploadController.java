package com.fc.test.controller.gen;

import io.swagger.annotations.Api;
import io.swagger.models.Model;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.UUID;

@Api(value = "资源上传下载")
@CrossOrigin
@RestController
public class UploadController {

    @Value("${uploadpath}")
     private String uploadPath;


    @PostMapping(value = "/fileUpload",produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String  fileUpload(@RequestParam(value = "file") MultipartFile file,@RequestParam(value = "file1") MultipartFile file1
            ,@RequestParam(value = "remark")String remark, HttpServletRequest request) throws IOException {

        if(!new File(uploadPath).exists()){
            new File(uploadPath).mkdirs();

        }
        String fileName=null;
        if(remark.equals("sp")){
            fileName = file.getOriginalFilename();  // 文件名
        }else{
            fileName = file1.getOriginalFilename();  // 文件名
        }
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        fileName = UUID.randomUUID() + suffixName; // 新文件名
        File dest = new File(uploadPath + fileName);
        try {
            if(remark.equals("sp")){
                file.transferTo(dest);
            }else{
                file1.transferTo(dest);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "/download?name=" + fileName;


    }



    @RequestMapping(value = "/download", method = RequestMethod.GET)
    @ResponseBody
    public void download(HttpServletResponse response,String name) {
        String file = uploadPath+name;
        try {
            FileInputStream inputStream = new FileInputStream(file);
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            String diskfilename = name;
//            response.setContentType("video/mp4");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + diskfilename + "\"");
            System.out.println("data.length " + data.length);
            response.setContentLength(data.length);
            response.setHeader("Content-Range", "" + Integer.valueOf(data.length - 1));
            response.setHeader("Accept-Ranges", "bytes");
            response.setHeader("Etag", "W/\"9767057-1323779115364\"");
            OutputStream os = response.getOutputStream();

            os.write(data);
            //先声明的流后关掉！
            os.flush();
            os.close();
            inputStream.close();

        } catch (Exception e) {

        }
    }






}
