package com.fc.test.mapper.auto;

import com.fc.test.model.auto.DeviceFaultDoc;
import com.fc.test.model.auto.DeviceFaultDocExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 设备故障文档 DeviceFaultDocMapper
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:45:49
 */
public interface DeviceFaultDocMapper {
      	   	      	      	      	      	      	      	      	      	      
    long countByExample(DeviceFaultDocExample example);

    int deleteByExample(DeviceFaultDocExample example);
		
    int deleteByPrimaryKey(String id);
		
    int insert(DeviceFaultDoc record);

    int insertSelective(DeviceFaultDoc record);

    List<DeviceFaultDoc> selectByExample(DeviceFaultDocExample example);
		
    DeviceFaultDoc selectByPrimaryKey(String id);
		
    int updateByExampleSelective(@Param("record") DeviceFaultDoc record, @Param("example") DeviceFaultDocExample example);

    int updateByExample(@Param("record") DeviceFaultDoc record, @Param("example") DeviceFaultDocExample example); 
		
    int updateByPrimaryKeySelective(DeviceFaultDoc record);

    int updateByPrimaryKey(DeviceFaultDoc record);
  	  	
}