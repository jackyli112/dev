package com.fc.test.mapper.auto;

import com.fc.test.model.auto.DeviceInstallDoc;
import com.fc.test.model.auto.DeviceInstallDocExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 设备安装文档 DeviceInstallDocMapper
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:02
 */
public interface DeviceInstallDocMapper {
      	   	      	      	      	      	      	      	      	      	      
    long countByExample(DeviceInstallDocExample example);

    int deleteByExample(DeviceInstallDocExample example);
		
    int deleteByPrimaryKey(String id);
		
    int insert(DeviceInstallDoc record);

    int insertSelective(DeviceInstallDoc record);

    List<DeviceInstallDoc> selectByExample(DeviceInstallDocExample example);
		
    DeviceInstallDoc selectByPrimaryKey(String id);
		
    int updateByExampleSelective(@Param("record") DeviceInstallDoc record, @Param("example") DeviceInstallDocExample example);

    int updateByExample(@Param("record") DeviceInstallDoc record, @Param("example") DeviceInstallDocExample example); 
		
    int updateByPrimaryKeySelective(DeviceInstallDoc record);

    int updateByPrimaryKey(DeviceInstallDoc record);
  	  	
}