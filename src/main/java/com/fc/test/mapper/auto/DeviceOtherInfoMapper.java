package com.fc.test.mapper.auto;

import com.fc.test.model.auto.DeviceOtherInfo;
import com.fc.test.model.auto.DeviceOtherInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 设备与单位信息 DeviceOtherInfoMapper
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:09
 */
public interface DeviceOtherInfoMapper {
      	   	      	      	      	      	      	      
    long countByExample(DeviceOtherInfoExample example);

    int deleteByExample(DeviceOtherInfoExample example);
		
    int deleteByPrimaryKey(String id);
		
    int insert(DeviceOtherInfo record);

    int insertSelective(DeviceOtherInfo record);

    List<DeviceOtherInfo> selectByExample(DeviceOtherInfoExample example);
		
    DeviceOtherInfo selectByPrimaryKey(String id);
		
    int updateByExampleSelective(@Param("record") DeviceOtherInfo record, @Param("example") DeviceOtherInfoExample example);

    int updateByExample(@Param("record") DeviceOtherInfo record, @Param("example") DeviceOtherInfoExample example); 
		
    int updateByPrimaryKeySelective(DeviceOtherInfo record);

    int updateByPrimaryKey(DeviceOtherInfo record);
  	  	
}