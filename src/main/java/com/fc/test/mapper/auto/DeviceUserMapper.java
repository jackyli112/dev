package com.fc.test.mapper.auto;

import com.fc.test.model.auto.DeviceUser;
import com.fc.test.model.auto.DeviceUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 普通用户 DeviceUserMapper
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:14
 */
public interface DeviceUserMapper {
      	   	      	      	      	      	      	      	      	      	      
    long countByExample(DeviceUserExample example);

    int deleteByExample(DeviceUserExample example);
		
    int deleteByPrimaryKey(String id);
		
    int insert(DeviceUser record);

    int insertSelective(DeviceUser record);

    List<DeviceUser> selectByExample(DeviceUserExample example);
		
    DeviceUser selectByPrimaryKey(String id);
		
    int updateByExampleSelective(@Param("record") DeviceUser record, @Param("example") DeviceUserExample example);

    int updateByExample(@Param("record") DeviceUser record, @Param("example") DeviceUserExample example); 
		
    int updateByPrimaryKeySelective(DeviceUser record);

    int updateByPrimaryKey(DeviceUser record);
  	  	
}