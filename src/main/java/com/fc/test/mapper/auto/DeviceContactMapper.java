package com.fc.test.mapper.auto;

import com.fc.test.model.auto.DeviceContact;
import com.fc.test.model.auto.DeviceContactExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 设备联系人 DeviceContactMapper
 * @author fuce_自动生成
 * @email 115889198@qq.com
 * @date 2022-07-17 20:46:19
 */
public interface DeviceContactMapper {
      	   	      	      	      	      	      	      	      	      	      	      
    long countByExample(DeviceContactExample example);

    int deleteByExample(DeviceContactExample example);
		
    int deleteByPrimaryKey(String id);
		
    int insert(DeviceContact record);

    int insertSelective(DeviceContact record);

    List<DeviceContact> selectByExample(DeviceContactExample example);
		
    DeviceContact selectByPrimaryKey(String id);
		
    int updateByExampleSelective(@Param("record") DeviceContact record, @Param("example") DeviceContactExample example);

    int updateByExample(@Param("record") DeviceContact record, @Param("example") DeviceContactExample example); 
		
    int updateByPrimaryKeySelective(DeviceContact record);

    int updateByPrimaryKey(DeviceContact record);
  	  	
}